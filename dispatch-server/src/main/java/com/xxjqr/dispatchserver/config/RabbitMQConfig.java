package com.xxjqr.dispatchserver.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableRabbit
public class RabbitMQConfig {
    //定义队列 订单队列
    @Bean
    public Queue orderQueue() {
        boolean durable = true;
        boolean exclusive = false;
        boolean autoDelete = false;
        Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("x-dead-letter-exchange","dead_order_exchange");
                put("x-dead-letter-routing-key","dead_order");
            }
        };
        return new Queue("order_queue", durable, exclusive, autoDelete,params);
    }

    //定义订单死信队列
    @Bean
    public Queue deadOrderQueue() {
        boolean durable = true;
        boolean exclusive = false;
        boolean autoDelete = false;
        return new Queue("dead_order_queue", durable, exclusive, autoDelete);
    }

    @Bean
    public DirectExchange directOrdertExchange() {
        boolean durable = true;
        boolean autoDelete = false;
        return new DirectExchange("direct_order_exchange", durable, autoDelete);
    }

    //定义死信交换机
    @Bean
    public DirectExchange deadOrdertExchange() {
        boolean durable = true;
        boolean autoDelete = false;
        return new DirectExchange("dead_order_exchange", durable, autoDelete);
    }

    //绑定死信队列和死信交换机
    @Bean
    public Binding deadOrderBinding() {
        return BindingBuilder
                .bind(deadOrderQueue())
                .to(deadOrdertExchange())
                .with("dead_order");
    }

    @Bean
    public Binding binding() {
        return BindingBuilder
                .bind(orderQueue())
                .to(directOrdertExchange())
                .with("order");
    }
}
