package com.xxjqr.dispatchserver.controller;

import com.xxjqr.dispatchserver.po.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class DispatchController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostMapping("dispatch")
    public String dispatch(@RequestBody Order order) throws InterruptedException {
        TimeUnit.SECONDS.sleep(3);
        //为了方便演示，就不写service层了
        String sql = "insert into t_dispatch values (?,?,?,?);";
        final int effectCount = jdbcTemplate.update(sql, (int) (Math.random() * 100), order.getId(), "超人", 0);
        if (effectCount > 0) {
            return "success";
        }
        return "fail";
    }
}
