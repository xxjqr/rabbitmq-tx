package com.xxjqr.dispatchserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DispatchServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DispatchServerApplication.class, args);
    }

}
