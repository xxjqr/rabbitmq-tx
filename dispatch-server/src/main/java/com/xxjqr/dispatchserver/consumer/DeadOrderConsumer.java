package com.xxjqr.dispatchserver.consumer;

import com.alibaba.fastjson.JSON;
import com.xxjqr.dispatchserver.po.Order;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@RabbitListener(queues = "dead_order_queue")
@Component
public class DeadOrderConsumer {

    @RabbitHandler
    public void reciveMessage(String message) {
        final Order order = JSON.parseObject(message, Order.class);
        System.out.println("死信队列收到了消息：" + message);
        System.out.println("发送消息告诉订单系统，该订单无法正常完成，让其修改is_delete 标识为 1");
    }
}
