package com.xxjqr.orderserver;

import com.xxjqr.orderserver.po.Order;
import com.xxjqr.orderserver.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class OrderServerApplicationTests {

    @Autowired
    private OrderService orderService;

    @Test
    void contextLoads() throws ClassNotFoundException {
        Order order = new Order();
        order.setId((int) (Math.random() * 100));
        order.setUserName("xxjqr");
        order.setProduct("肾宝");
        orderService.orderByRabbitMQ(order);
    }

}
